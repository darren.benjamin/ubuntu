# ubuntu

Various useful ubuntu scripts

### perform initial setup"
```sh
scripts/ubuntu-setup.sh
```

### vim configuration
```sh
cp vim/.vimrc ~
```

### git configuration
Suports multiple git configs
```sh
cp git/.gitconfig ~
```
