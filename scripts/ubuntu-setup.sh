echo "Install chrome"
wget wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i ./google-chrome-stable_current_amd64.deb

echo "apt install base packages"
sudo apt install -y git rwho net-tools jq unzip openssh-server ntp terminator vim-gnome make

echo "perform upgrade"
sudo apt -y upgrade
sudo apt -y autoremove

echo "snap install slack and remmina"
sudo snap install --classic slack
sudo snap install --edge authy
sudo snap install remmina
sudo snap connect remmina:avahi-observe :avahi-observe
sudo snap connect remmina:cups-control :cups-control
sudo snap connect remmina:mount-observe :mount-observe
sudo snap connect remmina:password-manager-service :password-manager-service

echo "refresh snap"
sudo snap refresh

echo "install latest nvidia drivers"
sudo ubuntu-drivers autoinstall
